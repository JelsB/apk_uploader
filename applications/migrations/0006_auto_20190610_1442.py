# Generated by Django 2.2.1 on 2019-06-10 12:42

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('applications', '0005_auto_20190610_1358'),
    ]

    operations = [
        migrations.AlterField(
            model_name='application',
            name='file',
            field=models.FileField(default='', upload_to=''),
        ),
    ]
