from .models import Application
from .serializers import ApplicationSerializer
from .forms import UploadApplicationForm
from rest_framework import generics
from django.shortcuts import redirect, render
from django.contrib import messages


# Create your views here.

class ApplicationList(generics.ListCreateAPIView):
    queryset = Application.objects.all()
    serializer_class = ApplicationSerializer


class ApplicationDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = Application.objects.all()
    serializer_class = ApplicationSerializer


def upload_app(request):
    if request.method == 'POST':
        form = UploadApplicationForm(request.POST, request.FILES)
        # TODO: Make sure the uploaded file is an apk extension
        if form.is_valid():
            form.save()
            messages.success(request,
                             'Application has been uploaded successfully!')
            return redirect('upload-app')
        else:
            messages.warning(request, 'Invalid file type.')

    else:
        form = UploadApplicationForm()
    return render(request, 'applications/upload_app.html', {'form': form})
