from django.urls import path
from .views import ApplicationList, ApplicationDetail, upload_app

urlpatterns = [
    path('', upload_app, name='upload-app'),
    path('applications/', ApplicationList.as_view(), name='applications'),
    path('applications/<int:pk>/', ApplicationDetail.as_view(), name='application-detail'),
]
