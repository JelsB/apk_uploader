from django.db import models
from django.utils import timezone
import subprocess
import re

# Create your models here.


class Application(models.Model):
    created = models.DateTimeField(default=timezone.now)
    application = models.CharField(max_length=100)
    package_name = models.CharField(max_length=100)
    package_version_code = models.CharField(max_length=100)
    file = models.FileField(default="")

    def save(self, *args, **kwargs):
        # call super save function to use uploaded file path later
        super().save(*args, **kwargs)
        # get metadata from uploaded file
        update_dict = self.extract_application_metadata()
        # update attributes
        self.application = self.file.url
        self.package_version_code = update_dict['version_code']
        self.package_name = update_dict['package_name']

        super().save(*args, **kwargs)

    def extract_application_metadata(self):
        info_dict = {'version_code': "versionCode=",
                     'package_name': "package: name=",
                     }

        # extract application metadata with aapt
        process = subprocess.run(['aapt', 'd', 'badging', f'{self.file.path}'],
                                 capture_output=True)
        string = process.stdout.decode()

        # extract string values of desired metadata
        for keys, values in info_dict.items():
            pattern = re.compile(f"{values}'(.*?)'")
            new_value = pattern.search(string).group(1)
            info_dict[keys] = new_value

        return info_dict

    class Meta:
        ordering = ('created',)
