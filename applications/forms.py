from django import forms
from django.core.exceptions import ValidationError
from .models import Application
import magic


class UploadApplicationForm(forms.ModelForm):
    file = forms.FileField()

    def clean_file(self):
        """Custom validation for file field"""
        # explicitly call from cleaned_data because clean()
        # has already been executed
        file = self.cleaned_data.get('file', False)
        filetype = magic.from_buffer(file.read(), mime=True)
        if not 'application/zip' in filetype:
            raise ValidationError(message='Invalid file type', code='invalid')
        return file

    class Meta:
        model = Application
        fields = ['file']
